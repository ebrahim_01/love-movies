package com.oxygen.o2.lovemovies;

/**
 * Created by O2 on 9/3/2015.
 */
public class UrlEndpoints {
    // public static final String API_KEY_ROTTEN_TOMATOES = "8895c1a9634c3e5da945a0c8621217af";
    public static final String URL_BOX_OFFICE = "https://api.themoviedb.org/3/discover/movie";
    public static final String URL_SEARCH = "https://api.themoviedb.org/3/search/movie";
    public static final String URL_CHAR_QUESTION = "?";
    public static final String URL_CHAR_AMEPERSAND = "&";
    public static final String URL_PARAM_API_KEY = "api_key=";
    public static final String URL_SEARCH_QUERY = "include_adult=true&query=";
    // public static final String URL_PARAM_LIMIT = "limit=";
    public static final String URL_RELEASE_DATE_GTE = "primary_release_date.gte=";
    public static final String URL_RELEASE_DATE_LTE = "primary_release_date.lte=";
    public static final String URL_SORT = "sort_by=release_date.desc";
    public static final String URL_US = "certification_country=US&certification=R";
    public static final String URL_PAGE = "page=";
    public static final String URL_LANGUAGE = "language=en";
    public static final String URL_HIGH_RATE = "certification_country=US&certification=R&sort_by=vote_average.desc";
    public static final String API_KEY_ROTTEN_TOMATOES = "8895c1a9634c3e5da945a0c8621217af";

}
