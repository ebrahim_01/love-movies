package com.oxygen.o2.lovemovies;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.io.File.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by O2 on 9/5/2015.
 */
class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;
    Bitmap bitmap;
 // Context context;
   // String posterName;
    public ImageDownloaderTask(ImageView imageView/*,Context context*/) {
        imageViewReference = new WeakReference<ImageView>(imageView);
    //  this.context=context;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
     /*   String posterpath=params[0].substring(23,params[0].indexOf("&"));
       posterName=posterpath.substring(0,posterpath.indexOf("."));
        File file=new File("/data/local/tmp/com.oxygen.o2.lovemovies/"+posterName+".txt");
      if(file.exists()) {
          FileInputStream fileInputStream = null;
          try {
              fileInputStream = context.openFileInput(posterName + ".txt");
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          }

          int read = -1;
          ArrayList<Byte> byteArrayList = new ArrayList<Byte>();
          byte[] bitmapArray = null;
          try {
              while ((read = fileInputStream.read()) != -1) {
                  byteArrayList.add((byte) read);

              }
              for (int i = 0; i < byteArrayList.size(); i++) {
                  bitmapArray[i] = byteArrayList.get(i);
              }
              bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
          } catch (IOException e) {
              e.printStackTrace();
          }
      }
        else{
                 URLConnection con = null;

                 try {
                     con = new URL(params[0]).openConnection();

                     con.setReadTimeout(1000 * 20);

                     bitmap = BitmapFactory.decodeStream((InputStream) con.getContent(), null, null);

                 } catch (IOException e1) {
                     e1.printStackTrace();
                 }
            if(SharedDate.cpg==1 && SharedDate.isNetAva())
                SaveToInternal();


        }*/


        URLConnection con = null;

        try {
            con = new URL(params[0]).openConnection();

            con.setReadTimeout(1000 * 20);

            bitmap = BitmapFactory.decodeStream((InputStream) con.getContent(), null, null);


        } catch (IOException e) {
            e.printStackTrace();
        }
       // return bitmap;

        return bitmap;
    }

   /* private void SaveToInternal() {
        ByteArrayOutputStream stream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] bitMapBytes=stream.toByteArray();
        String FileName=posterName+".txt";
        FileOutputStream fileOutputStream=null;
        try {
             fileOutputStream=context.openFileOutput(FileName,Context.MODE_PRIVATE);
           fileOutputStream.write(bitMapBytes);
            //fileOutputStream.flush();


        } catch (FileNotFoundException ee) {
            ee.printStackTrace();
        } catch (IOException eee) {
            eee.printStackTrace();
        }
        finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    @Override
    protected void onPostExecute(Bitmap bitmap) {


        ImageView imageView = imageViewReference.get();
        if (imageView != null) {

            imageView.setImageBitmap(bitmap);

        }

    }
}