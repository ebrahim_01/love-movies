package com.oxygen.o2.lovemovies;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import de.psdev.licensesdialog.LicensesDialog;
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20;
import de.psdev.licensesdialog.licenses.License;
import de.psdev.licensesdialog.model.Notice;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    SharedPreferences SaveData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        SaveData = getSharedPreferences("sharedData", 0);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);


    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        // Toast.makeText(this, "Menu item selected -> " + position, Toast.LENGTH_SHORT).show();
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
       /* fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();*/
        switch (position) {
            case 0:

                fragmentManager.beginTransaction()
                        .replace(R.id.container, BlankFragment_1.newInstance(position + 1))//*PlaceholderFragment.newInstance(position + 1)*/)
                        .commit();
                break;

            case 1:

                fragmentManager.beginTransaction()
                        .replace(R.id.container, BlankFragment_2.newInstance(position + 1))//*PlaceholderFragment.newInstance(position + 1)*/)
                        .commit();
                break;
           /* case 2:
                Intent intent=new Intent(this,ViewActivity.class);
                startActivity(intent);
              *//*  fragmentManager.beginTransaction()
                        .replace(R.id.container, BlankFragment_1.newInstance(position+1))*//*//**//*PlaceholderFragment.newInstance(position + 1)*//**//*)
                        .commit();*//*
                break;*/
        }
    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                showSearchDialog();
                break;
            case R.id.action_about:
                showLicenses();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showLicenses() {
        final String name = "Contact :";
        final String url = "ebrahim.ze01@gmail.com";
        final String copyright = "Copyright 2015 Ibrahim Taaloulou <br/> Facebook :<br/>http://facebook.com/ebrahim.ze";
        final License license = new ApacheSoftwareLicense20();
        final Notice notice = new Notice(name, url, copyright, license);
        new LicensesDialog.Builder(this).setNotices(notice).build().show();
    }

    public void showSearchDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView2 = layoutInflater.inflate(R.layout.search_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView2);
        final EditText search_word = (EditText) promptView2.findViewById(R.id.search_Edit_text);

        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("search", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (search_word.getText().toString().length() > 0) {
                            SharedPreferences.Editor editor = SaveData.edit();
                            String s = search_word.getText().toString().replace(" ", "%20");
                            editor.putString("search_title", s);
                            editor.commit();
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, BlankFragment_3.newInstance(3))//*PlaceholderFragment.newInstance(position + 1)*/)
                                    .commit();

                        } else {
                            dialog.cancel();
                        }
                    }
                })
                .setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

}
