package com.oxygen.o2.lovemovies;

/**
 * Created by O2 on 9/15/2015.
 */
public  class  SharedDate {
    public static int cpg=1;
    public static boolean netAva=false;

    public SharedDate() {
    }

    public static int getCpg() {
        return cpg;
    }

    public static void setCpg(int cpg) {
        SharedDate.cpg = cpg;
    }

    public static boolean isNetAva() {
        return netAva;
    }

    public static void setNetAva(boolean netAva) {
        SharedDate.netAva = netAva;
    }
}
