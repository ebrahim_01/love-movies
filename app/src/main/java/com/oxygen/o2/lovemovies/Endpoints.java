package com.oxygen.o2.lovemovies;

import java.util.Calendar;

import static com.oxygen.o2.lovemovies.UrlEndpoints.API_KEY_ROTTEN_TOMATOES;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_BOX_OFFICE;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_CHAR_AMEPERSAND;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_CHAR_QUESTION;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_HIGH_RATE;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_LANGUAGE;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_PAGE;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_PARAM_API_KEY;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_RELEASE_DATE_GTE;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_RELEASE_DATE_LTE;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_SEARCH;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_SEARCH_QUERY;
import static com.oxygen.o2.lovemovies.UrlEndpoints.URL_SORT;

/**
 * Created by O2 on 9/3/2015.
 */


public class Endpoints {

    public static final String getRequestUrlBoxOfficeMovies(int currentpage) {
        String cu = getCurrentDate();
        return URL_BOX_OFFICE
                + URL_CHAR_QUESTION
                + URL_RELEASE_DATE_GTE + "2015-08-02"
                + URL_CHAR_AMEPERSAND
                + URL_RELEASE_DATE_LTE + cu
                + URL_CHAR_AMEPERSAND
                + URL_PAGE + currentpage
                + URL_CHAR_AMEPERSAND
                + URL_LANGUAGE
                + URL_CHAR_AMEPERSAND
                + URL_SORT
                + URL_CHAR_AMEPERSAND
                + URL_PARAM_API_KEY + API_KEY_ROTTEN_TOMATOES;

    }

    public static String getHighRate(int currentpage) {
        return URL_BOX_OFFICE
                + URL_CHAR_QUESTION
                + URL_HIGH_RATE
                + URL_CHAR_AMEPERSAND
                + URL_PAGE + currentpage
                + URL_CHAR_AMEPERSAND
                + URL_SORT
                + URL_CHAR_AMEPERSAND
                + URL_PARAM_API_KEY + API_KEY_ROTTEN_TOMATOES;
    }

    public static String getSearchResult(String SearchWord, int currentpage) {
        return URL_SEARCH
                + URL_CHAR_QUESTION
                + URL_SEARCH_QUERY + SearchWord
                + URL_CHAR_AMEPERSAND
                + URL_PAGE + currentpage
                + URL_CHAR_AMEPERSAND
                + URL_PARAM_API_KEY + API_KEY_ROTTEN_TOMATOES;

    }

    public static String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String no = year + "-" + month + "-" + day;
        return no;
    }
}
