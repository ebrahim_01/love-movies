package com.oxygen.o2.lovemovies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;


public class ViewActivity extends ActionBarActivity {
    //  private Toolbar mToolbar;
    //ImageView im;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView image;
    TextView title, language, overView, reDate, voteCount;
    RatingBar sRatingBar;
    Context context;
    private static final String SERVER_ADDRESS = "http://image.tmdb.org/t/p/w300";
    private static final String API_KEY = "&api_key=8895c1a9634c3e5da945a0c8621217af";
    CardView cardView;

    // SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

      /*  mToolbar = (Toolbar) findViewById(R.id.app_bar);
        im=(ImageView)findViewById(R.id.header);
         setSupportActionBar(mToolbar);*/
        //  mSwipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swapToRefresh_v);
        //  mSwipeRefreshLayout.setOnRefreshListener(this);
        image = (ImageView) findViewById(R.id.image);
        image.setImageResource(R.drawable.wallpaper);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        cardView = (CardView) findViewById(R.id.cardView);
        YoYo.with(Techniques.SlideInDown)
                .duration(2000)
                .playOn(cardView);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(getIntent().getStringExtra("original_title"));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        setPalette();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title = (TextView) findViewById(R.id.original_title_v);
        language = (TextView) findViewById(R.id.language_v);
        overView = (TextView) findViewById(R.id.over_view_v);
        sRatingBar = (RatingBar) findViewById(R.id.ratingBar_t);
        reDate = (TextView) findViewById(R.id.release_date_v);
        voteCount = (TextView) findViewById(R.id.vote_count_v);
        title.setText(getIntent().getStringExtra("original_title"));
        language.setText(getIntent().getStringExtra("original_language"));
        overView.setText(getIntent().getStringExtra("overview"));
        reDate.setText(getIntent().getStringExtra("release_date"));
        voteCount.setText(getIntent().getStringExtra("vote_count"));
        sRatingBar.setRating(Float.parseFloat(getIntent().getStringExtra("vote_average")));
        sRatingBar.setAlpha(1.0F);
        String url = SERVER_ADDRESS + getIntent().getStringExtra("backdrop_path") /*+ API_KEY*/;
        new ImageDownloaderTask(image).execute(url);

    }

    private void setPalette() {
        Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                int primaryDark = getResources().getColor(R.color.myPrimaryDarkColor);
                int primary = getResources().getColor(R.color.myPrimaryColor);
                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(primary));
                collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkVibrantColor(primaryDark));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            String url = SERVER_ADDRESS + getIntent().getStringExtra("backdrop_path") + API_KEY;
            new ImageDownloaderTask(image).execute(url);
            return true;
        }
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }

  /*  public void change(View view) {
        //Context context;
       image.setImageResource(R.drawable.untitled);
    }*/

  /*  @Override
    public void onRefresh() {
        String url=SERVER_ADDRESS+getIntent().getStringExtra("backdrop_path")+API_KEY;
        new ImageDownloaderTask(image).execute(url);

        mSwipeRefreshLayout.setRefreshing(false);
    }*/
}
