package com.oxygen.o2.lovemovies;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment_3.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment_3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment_3 extends android.support.v4.app.Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    ArrayList<String> records3;
    Custom_Adapter mCustom_adapter3;
    ListView listProduct3;
    Activity context;
    ProgressDialog pd;
    SharedPreferences SaveData;
    int currentPage = 1;

    public BlankFragment_3() {
        // Required empty public constructor
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        StrictMode.ThreadPolicy Policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(Policy);
        context = getActivity();
        SaveData = getActivity().getSharedPreferences("sharedData", 0);
        records3 = new ArrayList<String>();
        Button loadmore = new Button(getActivity());
        loadmore.setText("Load more ...");
        listProduct3 = (ListView) getActivity().findViewById(R.id.search_list);
        mCustom_adapter3 = new Custom_Adapter(context, R.layout.item_list, R.id.Text_T, records3);
        //mCustom_adapter=new Custom_Adapter(context, R.layout.item_list,R.id.Text_T, records);
        listProduct3.setAdapter(mCustom_adapter3);
        listProduct3.addFooterView(loadmore);
        loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork == null) {
                    Toast.makeText(context, "Not connected to the internet", Toast.LENGTH_SHORT).show();

                } else {
                    currentPage += 1;
                    BackTask bt = new BackTask();
                    bt.execute();
                }
            }
        });
        listProduct3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] selected_movie = records3.get(position).split("__");
                //    Toast.makeText(context,""+selected_movie[0]+"\n"+selected_movie[1]+selected_movie[2]+"\n"+selected_movie[3]+"\n"+selected_movie[4]+"\n"+selected_movie[5]+"\n"+selected_movie[6]+"\n"+selected_movie[7], Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ViewActivity.class);
                //add data to the Intent object
                intent.putExtra("original_title", selected_movie[0]);
                intent.putExtra("poster_path", selected_movie[1]);
                intent.putExtra("vote_average", selected_movie[2]);
                intent.putExtra("release_date", selected_movie[3]);
                intent.putExtra("original_language", selected_movie[4]);
                intent.putExtra("backdrop_path", selected_movie[5]);
                intent.putExtra("overview", selected_movie[6]);
                intent.putExtra("vote_count", selected_movie[7]);


                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //execute background task
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) {
            Toast.makeText(context, "Not connected to the internet", Toast.LENGTH_SHORT).show();

        } else {
            currentPage = 1;
            BackTask bt = new BackTask();
            bt.execute();
        }
    }

    private class BackTask extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setTitle("Retrieving data");
            pd.setMessage("Please wait.");
            pd.setCancelable(true);
            pd.setIndeterminate(true);
            pd.show();

        }

        protected Void doInBackground(Void... params) {

            InputStream is = null;
            InputStream is2 = null;
            String result = "";
            String search_word = "";
            //"http://api.themoviedb.org/3/discover/movie?primary_release_date.gte=2015-08-02&primary_release_date.lte=2015-09-02&api_key=8895c1a9634c3e5da945a0c8621217af");
          /*  List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(1);
            nameValuePair.add(new BasicNameValuePair("primary_release_date.gte","2015-08-02"));
            nameValuePair.add(new BasicNameValuePair("primary_release_date.lte","2015-09-02"));
            nameValuePair.add(new BasicNameValuePair("api_key","8895c1a9634c3e5da945a0c8621217af"));*/
            try {
                search_word = SaveData.getString("search_title", "").toString();
                URLConnection con = new URL(Endpoints.getSearchResult(search_word, currentPage)).openConnection();
                con.setConnectTimeout(1000 * 20);
                con.setReadTimeout(1000 * 20);

                is2 = (InputStream) con.getContent();
                // httpclient = createHttpClient();
            /* httpclient=Singleton_con.getInstance().getHttpclient();
                // edit
                httppost= Singleton_con.getInstance().getHttppost();
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                response=httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();*/

            } catch (Exception e) {

                if (pd != null)
                    pd.dismiss();  //close the dialog if error occurs
                // Log.e("ERROR", e.toString());
                Toast.makeText(context, "problem in the server...", Toast.LENGTH_SHORT).show();
            }


            //convert response to string
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is2, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is2.close();
                result = sb.toString();
            } catch (Exception e) {
                Log.e("ERROR", "Error converting result " + e.toString());

            }

            //parse json data
            try {
                records3.clear();
                // Toast.makeText(context,""+result,Toast.LENGTH_LONG).show();
                JSONObject jo = new JSONObject(result);
                /*JSONArray jArray =new JSONArray(result);*/
                JSONArray jArray = jo.getJSONArray("results");
                for (int i = 0; i < 5; i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    //release_date
                    String record = json_data.getString("original_title") + "__" + json_data.getString("poster_path") + "__" + json_data.getDouble("vote_average") + "__" + json_data.getString("release_date") + "__" + json_data.getString("original_language") + "__" + json_data.getString("backdrop_path") + "__" + json_data.getString("overview") + "__" + json_data.getInt("vote_count");
                    records3.add(record);


                }


            } catch (Exception e) {
                Log.e("ERROR", "Error pasting data " + e.toString());

            }

            // return null;

            return null;
        }


        protected void onPostExecute(Void result) {

            if (pd != null) pd.dismiss(); //close dialog
            if (records3.isEmpty())
                Toast.makeText(context, "NO result found !!!", Toast.LENGTH_SHORT).show();
            mCustom_adapter3.notifyDataSetChanged(); //notify the ListView to get new records


        }

    }

    public static BlankFragment_3 newInstance(int sectionNumber) {
        BlankFragment_3 fragment = new BlankFragment_3();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank_fragment_3, container, false);
    }


}
