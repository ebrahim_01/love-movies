package com.oxygen.o2.lovemovies;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by O2 on 3/14/2015.
 */
public class SplashScreen extends Activity {
    // ProgressBar pgr;
    // int progress=0;
    ImageView imageView;
    //  ViewGroup viewGroup;
    android.os.Handler h = new android.os.Handler();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);
        // احذف اشارة // من السطر في الأسفل لتمكينه .. إذا اردت أن يتصل مع واجهة لياوت

        requestWindowFeature(Window.FEATURE_NO_TITLE); // اخفاء الاكشن بار
        //if (Build.VERSION.SDK_INT < 16) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //}

        setContentView(R.layout.activity_splash);
        //  viewGroup= (ViewGroup) findViewById(R.id.spl_act);
        //  pgr=(ProgressBar)findViewById(R.id.progrss1);
        imageView = (ImageView) findViewById(R.id._logo);
     /*   final Explode explode=new Explode();
        explode.setDuration(1000);*/
        YoYo.with(Techniques.FadeInUp)
                .duration(2000)
                .playOn(imageView);

        //   TransitionManager.beginDelayedTransition(viewGroup,explode);
        //   getWindow().setExitTransition(explode);

        /****** انشاء ثريد لجعل التطبيق ينام لمدة 5 ثواني  *************/
        Thread background = new Thread() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            public void run() {
                //for progress bar
              /*  for(int i=0;i<5;i++)
                {
                    progress+=20;
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            pgr.setProgress(progress);
                            if(progress==pgr.getMax())
                            {
                                pgr.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }*/


                //**
                try {

                    sleep(3 * 1000); // غير في الرقم 5 لتغيير عدد الثواني .. ثواني الانتظار قبل الدخول للتطبيق

                    // بعد 5 ثواني نفذ التالي وهو الانتقال بنا الى الشاشة الرئيسية
                    Intent ii = new Intent(SplashScreen.this, MainActivity.class);

                    startActivity(ii);

                    // اغلاق شاشة السبلاش بشكل كلشي بعد الانتقال
                    finish();

                } catch (Exception e) {

                }
            }
        };

        // تشغيل الثريد السابق

        background.start();
    }

}
