package com.oxygen.o2.lovemovies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by O2 on 8/29/2015.
 */
public class Custom_Adapter extends ArrayAdapter<String> {
    int groupid;
    ArrayList<String> records;
    Context context;
    Bitmap bitmap;
    URLConnection con;

    /*http://myfirstdomain.comuv.com/image_pull_test/get_image.php
    http://10.0.2.2/image_pull_test/*/
    private static final String SERVER_ADDRESS = "http://image.tmdb.org/t/p/w92";
    private static final String API_KEY = "&api_key=8895c1a9634c3e5da945a0c8621217af";

    public Custom_Adapter(Context context, int vg, int id, ArrayList<String> records) {
        super(context, vg, id, records);
        this.context = context;
        groupid = vg;
        this.records = records;

    }

/*ImageLoader getLoader()
{
      mImageLoader=new ImageLoader(mRequestQueue,new ImageLoader.ImageCache() {

            private LruCache<String, Bitmap> cache=new LruCache<>((int)(Runtime.getRuntime().maxMemory()/1024)/8);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });}*/

    public View getView(final int position, final View convertView, ViewGroup parent) {

        final ViewHolder vh = new ViewHolder();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View itemView = inflater.inflate(groupid, parent, false);
        vh.imageView = (ImageView) itemView.findViewById(R.id.list_image);
        vh.TitleDateView = (TextView) itemView.findViewById(R.id.Text_T);
        vh.mRatingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
        vh.TitleDateView2 = (TextView) itemView.findViewById(R.id.Text_T_3);
        String[] row_items = records.get(position).split("__");
        //String url = SERVER_ADDRESS + row_items[1] + API_KEY;
        String url = SERVER_ADDRESS + row_items[1];

        vh.TitleDateView.setText(row_items[0]);
        vh.mRatingBar.setRating(Float.parseFloat(row_items[2]));
        vh.mRatingBar.setAlpha(1.0F);
        vh.TitleDateView2.setText(row_items[3]);

        new ImageDownloaderTask(vh.imageView).execute(url);
        return itemView;


    }


    public Bitmap getBitmap(String url) {

        try {
            URLConnection con = new URL(url).openConnection();
            con.setReadTimeout(1000 * 20);
            bitmap = BitmapFactory.decodeStream((InputStream) con.getContent(), null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    static class ViewHolder {
        RatingBar mRatingBar;
        TextView TitleDateView;
        TextView TitleDateView2;
        ImageView imageView;
    }
}
