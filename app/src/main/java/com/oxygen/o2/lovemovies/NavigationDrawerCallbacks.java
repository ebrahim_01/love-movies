package com.oxygen.o2.lovemovies;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
